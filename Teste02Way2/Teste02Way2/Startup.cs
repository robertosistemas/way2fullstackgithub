﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Teste02Way2.Startup))]
namespace Teste02Way2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
