﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Teste02Way2.App_Start.Codigo;
using Teste02Way2.Models;

namespace Teste02Way2.Controllers
{
    public class GitHubController : Controller
    {
        private GitHubRepositorioDAL githubrepo = new GitHubRepositorioDAL();

        /// <summary>
        /// Retorna uma lista contendo todos os repositórios do usuário informado
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string nomeUsuario = "robertosistemas";
            List<GitHubRepositorio> lista = null;
            try
            {
                lista = githubrepo.obterRepositoriosCandidato(nomeUsuario);
                return View(lista);
            }
            catch (Exception e)
            {
                HandleErrorInfo modelo = new HandleErrorInfo(e, "GitHub", "Index");
                return View("Error", modelo);
            }
        }

        /// <summary>
        /// Procura por repositórios por parte do nome ou o nome completo
        /// </summary>
        /// <param name="parteNome"></param>
        /// <returns></returns>
        public ActionResult ProcurarRepositorios(string parteNome)
        {
            string parteNomeRepositorio = string.IsNullOrWhiteSpace(parteNome) ? string.Empty : parteNome;
            ViewBag.ParteNome = parteNomeRepositorio;
            List<GitHubRepositorio> lista = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(parteNomeRepositorio))
                {
                    lista = githubrepo.procurarRepositorios(parteNomeRepositorio);
                }
                else
                {
                    lista = new List<GitHubRepositorio>();
                }
                return View(lista);
            }
            catch (Exception e)
            {
                HandleErrorInfo modelo = new HandleErrorInfo(e, "GitHub", "ProcurarRepositorios");
                return View("Error", modelo);
            }
        }

        /// <summary>
        /// Retorna lista de repositórios favoritos dos usuários que utilizam os sistemas do usuário informado
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public ActionResult RepositoriosFavoritos()
        {
            List<GitHubRepositorio> lista = null;
            try
            {
                lista = githubrepo.obterRepositoriosFavoritos();
                return View(lista);
            }
            catch (Exception e)
            {
                HandleErrorInfo modelo = new HandleErrorInfo(e, "GitHub", "RepositoriosFavoritos");
                return View("Error", modelo);
            }
        }

        /// <summary>
        /// Retorna modelo de detalhes sendo possível salvar como favorito ou excluir
        /// </summary>
        /// <param name="usuario">Usuário</param>
        /// <param name="repositorio">Repositório</param>
        /// <returns></returns>
        public ActionResult Detalhes(string usuario, string repositorio)
        {
            try
            {
                GitHubRepositorio gitHubRepositorio = githubrepo.obterRepositorio(usuario, repositorio);
                if (gitHubRepositorio == null)
                {
                    HandleErrorInfo modelo = new HandleErrorInfo(null, "GitHub", "Detalhes");
                    return View("Error", modelo);
                }

                // Seta propriedade indicando já foi salvo ou não nos favoritos
                gitHubRepositorio.JaSalvo = githubrepo.verificarJaSalvo(usuario, repositorio);

                return View(gitHubRepositorio);
            }
            catch (Exception e)
            {
                HandleErrorInfo modelo = new HandleErrorInfo(e, "GitHub", "Detalhes");
                return View("Error", modelo);
            }
        }

        /// <summary>
        /// Salva repositório como favorito
        /// </summary>
        /// <param name="usuario">Usuário</param>
        /// <param name="repositorio">Repositório</param>
        /// <returns></returns>
        [HttpPost, HttpParamAction]
        [ValidateAntiForgeryToken]
        public ActionResult SalvarRepositorio(string usuario, string repositorio)
        {
            try
            {
                githubrepo.salvarRepositorioFavorito(usuario, repositorio);
                return RedirectToAction("RepositoriosFavoritos");
            }
            catch (Exception e)
            {
                HandleErrorInfo modelo = new HandleErrorInfo(e, "GitHub", "SalvarRepositorio");
                return View("Error", modelo);
            }
        }

        /// <summary>
        /// Excluir repositório dos favoritos
        /// </summary>
        /// <param name="usuario">Usuário</param>
        /// <param name="repositorio">Repositório</param>
        /// <returns></returns>
        [HttpPost, HttpParamAction]
        [ValidateAntiForgeryToken]
        public ActionResult ExcluirRepositorio(string usuario, string repositorio)
        {
            try
            {
                githubrepo.excluirRepositorioFavorito(usuario, repositorio);
                return RedirectToAction("RepositoriosFavoritos");
            }
            catch (Exception e)
            {
                HandleErrorInfo modelo = new HandleErrorInfo(e, "GitHub", "ExcluirRepositorio");
                return View("Error", modelo);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                githubrepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
