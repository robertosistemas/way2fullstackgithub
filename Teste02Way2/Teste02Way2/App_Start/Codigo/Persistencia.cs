﻿using System;
using System.IO;

namespace Teste02Way2.Models
{
    public class Persistencia
    {

        private static object SynckLock = new object();

        public static string carregaArquivo(string arquivo)
        {
            try
            {
                lock (SynckLock)
                {
                    using (StreamReader sr = new StreamReader(arquivo))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar carregar arquivo.", e);
            }
        }

        public static void salvaArquivo(string arquivo, string json)
        {
            try
            {
                lock (SynckLock)
                {
                    using (StreamWriter sw = new StreamWriter(arquivo))
                    {
                        sw.Write(json);
                    }
                }
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar salvar arquivo.", e);
            }
        }

        public static string CaminhoCompleto(string arquivo)
        {
            try
            {
                object diretorio = AppDomain.CurrentDomain.GetData("DataDirectory");
                string caminho = System.IO.Path.Combine(diretorio.ToString(), arquivo);
                return caminho;
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar obter o caminho completo do arquivo.", e);
            }
        }
    }
}
