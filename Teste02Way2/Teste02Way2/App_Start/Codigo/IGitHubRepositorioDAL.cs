﻿using System.Collections.Generic;

namespace Teste02Way2.Models
{
    public interface IGitHubRepositorioDAL
    {
        List<GitHubRepositorio> obterRepositoriosCandidato(string usuario);
        List<GitHubRepositorio> procurarRepositorios(string parteNome);
        List<GitHubRepositorio> obterRepositoriosFavoritos();
        GitHubRepositorio obterRepositorio(string usuario, string repositorio);
        void salvarRepositorioFavorito(string usuario, string repositorio);
        void excluirRepositorioFavorito(string usuario, string repositorio);
        bool verificarJaSalvo(string usuario, string repositorio);
    }
}
