﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Teste02Way2.Models
{
    public class GitHubREST
    {

        // Utilizada para fazer ou não cache de consulta localmente
        public static bool UtilizarCacheLocalParaTestes = false;

        /// <summary>
        /// Obtem lista de repositórios de um usuário de modo síncrono
        /// </summary>
        /// <param name="usuario">Usuário</param>
        /// <returns></returns>
        public static List<GitHubRepositorio> obterRepositoriosUsuario(string usuario)
        {
            try
            {
                List<GitHubRepositorio> repositorios = null;
                string json = consultaAPI(string.Format("users/{0}/repos", usuario));
                if (!string.IsNullOrWhiteSpace(json))
                {
                    repositorios = JsonConvert.DeserializeObject<List<GitHubRepositorio>>(json);
                }
                return repositorios;
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar obter repositórios do usuário.", e);
            }
        }

        /// <summary>
        /// Pesquisa por repositórios por partes do nome de modo síncrono
        /// </summary>
        /// <param name="parteNome">Parte do nome do repositório</param>
        /// <returns></returns>
        public static List<GitHubRepositorio> procurarRepositorios(string parteNome)
        {
            try
            {
                GitHubListaRepositorio repositorios = null;
                string json = consultaAPI(string.Format("search/repositories?q={0}", parteNome));
                if (!string.IsNullOrWhiteSpace(json))
                {
                    repositorios = JsonConvert.DeserializeObject<GitHubListaRepositorio>(json);
                }
                return repositorios.items;
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar procurar por repositórios por partes do nome.", e);
            }
        }

        /// <summary>
        /// Obtem a lista contribuidores
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public static List<GitHubProprietario> obterContribuidores(string usuario, string repositorio)
        {
            try
            {
                List<GitHubProprietario> contribuidores = null;
                string json = consultaAPI(string.Format("repos/{0}/{1}/contributors", usuario, repositorio));
                if (!string.IsNullOrWhiteSpace(json))
                {
                    contribuidores = JsonConvert.DeserializeObject<List<GitHubProprietario>>(json);
                }
                return contribuidores;
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar obter Contribuidores.", e);
            }
        }

        /// <summary>
        /// Obtem informações de um repositório, e lista de contribuídores
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public static GitHubRepositorio obterRepositorio(string usuario, string repositorio)
        {
            try
            {
                GitHubRepositorio repositorioPesquisado = null;
                string json = consultaAPI(string.Format("repos/{0}/{1}", usuario, repositorio));

                if (!string.IsNullOrWhiteSpace(json))
                {
                    repositorioPesquisado = JsonConvert.DeserializeObject<GitHubRepositorio>(json);
                }

                if (repositorioPesquisado != null)
                {
                    // Atualiza lista de Contribuídores
                    repositorioPesquisado.Contribuidores = obterContribuidores(usuario, repositorio);
                }
                return repositorioPesquisado;
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar obter repositório.", e);
            }
        }

        /// <summary>
        /// Consulta a API REST do GitHub recebendo como parâmetro um segmento de URL
        /// </summary>
        /// <param name="segmento"></param>
        /// <returns></returns>
        private static string consultaAPI(string segmento)
        {

            try
            {

                string respostaDoServidor = string.Empty;
                string requestQueryString = string.Format("https://api.github.com/{0}", segmento);

                string arquivo = Persistencia.CaminhoCompleto(string.Format("{0}.json", Util.GetMd5Hash(requestQueryString)));

                // Verifica se vai fazer cache local das consultas na API do github no ambiente de testes para acelerar as pesquisas
                // Se estiver fazendo cache, consulta o conteúdo do arquivo localmente
                if (UtilizarCacheLocalParaTestes && System.IO.File.Exists(arquivo))
                {
                    respostaDoServidor = Persistencia.carregaArquivo(arquivo);
                }
                else
                {

                    // Consulta a API do GitHub

                    ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

                    HttpWebRequest request = WebRequest.Create(requestQueryString) as HttpWebRequest;

                    request.UserAgent = "Teste02Way2";
                    request.Method = "GET";
                    request.ServicePoint.Expect100Continue = false;
                    request.Credentials = CredentialCache.DefaultCredentials;
                    request.Timeout = 300000;

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(dataStream))
                            {
                                respostaDoServidor = reader.ReadToEnd();
                            }
                        }
                    }

                    // Verifica se vai fazer cache local das consultas na API do github no ambiente de testes para acelerar as pesquisas
                    // Se estiver fazendo cache, salva o conteúdo do arquivo localmente
                    if (UtilizarCacheLocalParaTestes)
                    {
                        Persistencia.salvaArquivo(arquivo, respostaDoServidor);
                    }

                }

                return respostaDoServidor;
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao consultar API.", e);
            }
        }

        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

    }
}
