﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Teste02Way2.Models
{
    public class Util
    {
        public static string GetMd5Hash(string input)
        {
            try
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
                    StringBuilder sBuilder = new StringBuilder();
                    for (int i = 0; i < data.Length; i++)
                    {
                        sBuilder.Append(data[i].ToString("x2"));
                    }
                    return sBuilder.ToString();
                }
            }
            catch (Exception e)
            {
                throw new GitHubException("Erro ao tentar carregar obter o Md5 Hash.", e);
            }
        }
    }
}
