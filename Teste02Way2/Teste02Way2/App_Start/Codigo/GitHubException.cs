﻿using System;

namespace Teste02Way2.Models
{
    public class GitHubException : Exception
    {
        public GitHubException() : base()
        {

        }

        public GitHubException(String message) : base(message)
        {

        }

        public GitHubException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public GitHubException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {

        }
    }
}
