﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Teste02Way2.Models
{
    public class GitHubRepositorioDAL : IGitHubRepositorioDAL, IDisposable
    {

        public List<GitHubRepositorio> obterRepositoriosCandidato(string usuario)
        {
            return GitHubREST.obterRepositoriosUsuario(usuario);
        }

        public List<GitHubRepositorio> procurarRepositorios(string parteNome)
        {
            return GitHubREST.procurarRepositorios(parteNome);
        }

        public GitHubRepositorio obterRepositorio(string usuario, string repositorio)
        {
            return GitHubREST.obterRepositorio(usuario, repositorio);
        }

        public List<GitHubRepositorio> obterRepositoriosFavoritos()
        {
            string arquivo = Persistencia.CaminhoCompleto("Favoritos.json");
            string json = string.Empty;

            if (System.IO.File.Exists(arquivo))
            {
                json = Persistencia.carregaArquivo(arquivo);
            }

            List<GitHubRepositorio> lista = null;

            if (!string.IsNullOrWhiteSpace(json))
            {
                lista = JsonConvert.DeserializeObject<List<GitHubRepositorio>>(json);
            }
            else
            {
                lista = new List<GitHubRepositorio>();
            }
            return lista;
        }

        public void salvarRepositorioFavorito(string usuario, string repositorio)
        {

            // Pesquisa no github pelo repositório

            GitHubRepositorio repo = obterRepositorio(usuario, repositorio);

            if (repo != null)
            {

                // Obtêm os favoritos salvos
                List<GitHubRepositorio> lista = obterRepositoriosFavoritos();

                // Procura na lista para saber se o repositório já está salvos
                GitHubRepositorio itemLista = lista.Find((item) => item.id == repo.id);

                // Procura na lista para saber se o repositório já está salvos
                if (itemLista != null)
                {
                    // Se encontrou, então remove para adicionar o mais atualizado
                    lista.Remove(itemLista);
                }

                // adiciona o novo repositório
                lista.Add(repo);

                // Salva arquivo
                salvarFavoritos(lista);

            }

        }

        public void excluirRepositorioFavorito(string usuario, string repositorio)
        {
            // Obtêm os favoritos salvos
            List<GitHubRepositorio> lista = obterRepositoriosFavoritos();

            // Procura na lista para saber se o repositório já está salvos
            GitHubRepositorio itemLista = lista.Find((item) => string.Compare(item.owner.login, usuario, true) == 0 && string.Compare(item.name, repositorio, true) == 0);

            // Procura na lista para saber se o repositório já está salvos
            if (itemLista != null)
            {
                // Se encontrou, então remove para adicionar o mais atualizado
                lista.Remove(itemLista);
            }

            // Salva arquivo
            salvarFavoritos(lista);
        }

        public bool verificarJaSalvo(string usuario, string repositorio)
        {
            // Procura na lista para saber se o repositório já está salvos
            return obterRepositoriosFavoritos().Exists((item) => string.Compare(item.owner.login, usuario, true) == 0 && string.Compare(item.name, repositorio, true) == 0);
        }

        private void salvarFavoritos(List<GitHubRepositorio> lista)
        {
            //Classifica a lista pelo nome e Serializa
            string json = JsonConvert.SerializeObject(lista.OrderBy((item) => item.name).ToList());

            // Obtêm o nome do arquivo para persistência
            string arquivo = Persistencia.CaminhoCompleto("Favoritos.json");

            // Salva arquivo
            Persistencia.salvaArquivo(arquivo, json);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GitHubRepositorioDAL() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
