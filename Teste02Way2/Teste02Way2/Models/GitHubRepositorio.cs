﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Teste02Way2.Models
{
    public class GitHubRepositorio
    {
        [Display(Name = "Id")]
        public long id { get; set; }
        [Display(Name = "Nome")]
        public string name { get; set; }
        [Display(Name = "Nome completo")]
        public string full_name { get; set; }

        [Display(Name = "Proprietário")]
        public GitHubProprietario owner;

        [Display(Name = "Privado")]
        [JsonProperty("private")]
        public bool IsPrivate { get; set; }

        [Display(Name = "Url html")]
        public string html_url { get; set; }
        [Display(Name = "Descrição")]
        public string description { get; set; }
        [Display(Name = "fork")] //traduzir
        public bool fork { get; set; }
        [Display(Name = "Url")]
        public string url { get; set; }
        [Display(Name = "Url forks")] //traduzir
        public string forks_url { get; set; }
        [Display(Name = "Url chaves")]
        public string keys_url { get; set; }
        [Display(Name = "Url Contribuíntes")]
        public string collaborators_url { get; set; }
        [Display(Name = "Url Time")]
        public string teams_url { get; set; }
        [Display(Name = "Url hooks")] //traduzir
        public string hooks_url { get; set; }
        [Display(Name = "Url eventos tópico")]
        public string issue_events_url { get; set; }
        [Display(Name = "Url eventos")]
        public string events_url { get; set; }
        [Display(Name = "Url assignees")] //traduzir
        public string assignees_url { get; set; }
        [Display(Name = "Url Troncos")]
        public string branches_url { get; set; }
        [Display(Name = "Url Tags")]
        public string tags_url { get; set; }
        [Display(Name = "Url Blobs")]
        public string blobs_url { get; set; }
        [Display(Name = "Url Tags GIT")]
        public string git_tags_url { get; set; }
        [Display(Name = "Url Ref. GIT")]
        public string git_refs_url { get; set; }
        [Display(Name = "Url árvore")]
        public string trees_url { get; set; }
        [Display(Name = "Url usuários iniciantes")] //traduzir
        public string statuses_url { get; set; }
        [Display(Name = "Url Linguagens")]
        public string languages_url { get; set; }
        [Display(Name = "Url Estrelados")] //traduzir
        public string stargazers_url { get; set; }
        [Display(Name = "Url contribuíntes")]
        public string contributors_url { get; set; }
        [Display(Name = "Url Subinscritos")]
        public string subscribers_url { get; set; }
        [Display(Name = "Url Subinscrição")]
        public string subscription_url { get; set; }
        [Display(Name = "Url Commits")]
        public string commits_url { get; set; }
        [Display(Name = "Url Commits GIT")]
        public string git_commits_url { get; set; }
        [Display(Name = "Url Comentário")]
        public string comments_url { get; set; }
        [Display(Name = "Url Comentário Tópico")]
        public string issue_comment_url { get; set; }
        [Display(Name = "Url Conteúdo")]
        public string contents_url { get; set; }
        [Display(Name = "Url Comparação")]
        public string compare_url { get; set; }
        [Display(Name = "Url Merges")]
        public string merges_url { get; set; }
        [Display(Name = "Url Arquivamento")]
        public string archive_url { get; set; }
        [Display(Name = "Url Downloads")]
        public string downloads_url { get; set; }
        [Display(Name = "Url Tópicos")]
        public string issues_url { get; set; }
        [Display(Name = "Url Pulls")] //traduzir
        public string pulls_url { get; set; }
        [Display(Name = "Url Milestones")] //traduzir
        public string milestones_url { get; set; }
        [Display(Name = "Url Notificações")]
        public string notifications_url { get; set; }
        [Display(Name = "Url Rótulos")]
        public string labels_url { get; set; }
        [Display(Name = "Url Releases")]
        public string releases_url { get; set; }
        [Display(Name = "Url distribuições")]
        public string deployments_url { get; set; }
        [Display(Name = "Criado em")]
        public string created_at { get; set; }
        [Display(Name = "Atualizado em")]
        public string updated_at { get; set; }
        [Display(Name = "Emputado em")]
        public string pushed_at { get; set; }
        [Display(Name = "Url Git")]
        public string git_url { get; set; }
        [Display(Name = "Url SSH")]
        public string ssh_url { get; set; }
        [Display(Name = "Url Clonar")]
        public string clone_url { get; set; }
        [Display(Name = "Url SVN")]
        public string svn_url { get; set; }
        [Display(Name = "Home Page")]
        public string homepage { get; set; }
        [Display(Name = "Tamanho")]
        public long size { get; set; }
        [Display(Name = "Contador de Estrelas")] //traduzir
        public long stargazers_count { get; set; }
        [Display(Name = "Contador Visualizações")]
        public long watchers_count { get; set; }
        [Display(Name = "Linguagem")]
        public string language { get; set; }
        [Display(Name = "Tem Tópico")]
        public bool has_issues { get; set; }
        [Display(Name = "Tem Downloads")]
        public bool has_downloads { get; set; }
        [Display(Name = "Tem Wiki")]
        public bool has_wiki { get; set; }
        [Display(Name = "Tem página")]
        public bool has_pages { get; set; }
        [Display(Name = "forks_count")] //traduzir
        public long forks_count { get; set; }
        [Display(Name = "Url Espelho")]
        public string mirror_url { get; set; }
        [Display(Name = "Contador tópicos abertos")]
        public long open_issues_count { get; set; }
        [Display(Name = "forks")] //traduzir
        public long forks { get; set; }
        [Display(Name = "Tópicos abertos")]
        public long open_issues { get; set; }
        [Display(Name = "Visualizações")]
        public long watchers { get; set; }
        [Display(Name = "Ramo Padrão")]
        public string default_branch { get; set; }

        [Display(Name = "Classificação")]
        [JsonIgnore()]
        public double score { get; set; } //Opcional

        [JsonIgnore]
        [Display(Name = "Contribuídores")]
        public List<GitHubProprietario> Contribuidores { get; set; }

        [JsonIgnore]
        [Display(Name = "Já está Salvo")]
        public bool JaSalvo { get; set; }

    }
}