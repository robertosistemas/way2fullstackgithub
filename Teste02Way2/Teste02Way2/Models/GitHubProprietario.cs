﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Teste02Way2.Models
{

    public class GitHubProprietario
    {
        [Display(Name = "Usuário")]
        public string login { get; set; }

        [Display(Name = "Id")]
        public long id { get; set; }

        [Display(Name = "Url Avatar")]
        public string avatar_url { get; set; }

        [Display(Name = "Id Gravatar")]
        public string gravatar_id { get; set; }

        [Display(Name = "Url")]
        public string url { get; set; }

        [Display(Name = "Url Html")]
        public string html_url { get; set; }

        [Display(Name = "Url Seguidores")]
        public string followers_url { get; set; }

        [Display(Name = "Url Seguindo")]
        public string following_url { get; set; }

        [Display(Name = "Url Gists")]
        public string gists_url { get; set; }

        [Display(Name = "Url Estrelas")]
        public string starred_url { get; set; }

        [Display(Name = "Url Inscrições")]
        public string subscriptions_url { get; set; }

        [Display(Name = "Url Organizações")]
        public string organizations_url { get; set; }

        [Display(Name = "Url Repositórios")]
        public string repos_url { get; set; }

        [Display(Name = "Url Eventos")]
        public string events_url { get; set; }

        [Display(Name = "Url Eventos Recebidos")]
        public string received_events_url { get; set; }

        [Display(Name = "Tipo")]
        public string type { get; set; }

        [Display(Name = "Administrador Site")]
        public bool site_admin { get; set; }

        // Usado na lista de contribuidores
        [Display(Name = "Contribuições")]
        public long contributions { get; set; }

        [JsonIgnore]
        [Display(Name = "Favoritos")]
        public List<GitHubRepositorio> Favoritos { get; set; }
    }
}
