﻿using System.Collections.Generic;

namespace Teste02Way2.Models
{
    public class GitHubListaRepositorio
    {
        public long total_count { get; set; }
        public bool incomplete_results { get; set; }
        public List<GitHubRepositorio> items { get; set; }
    }
}
