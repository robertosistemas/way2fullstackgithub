﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Teste02Way2.Models;
using Teste02Way2.Tests.Classes;

namespace Teste02Way2.Tests.Controllers
{
    [TestClass]
    public class GitHubRESTTest
    {

        /// <summary>
        /// Inicializa informações necessárias
        /// </summary>
        [TestInitialize]
        public void Inicializa()
        {
            // True para fazer cache local e False para sempre buscar no GitHub
            GitHubREST.UtilizarCacheLocalParaTestes = true;

            FuncoesTestes.InicializaCaminho();
        }

        /// <summary>
        /// Testa o a API que retorna a lista de repositório de um usuário
        /// </summary>
        [TestMethod]
        public void obterRepositoriosUsuario()
        {
            List<GitHubRepositorio> lista = GitHubREST.obterRepositoriosUsuario("robertosistemas");
            Assert.IsTrue(lista.Count > 0);

        }

        /// <summary>
        /// Testa o a API que procura repositórios por partes do nome
        /// </summary>
        [TestMethod]
        public void procurarRepositorios()
        {
            List<GitHubRepositorio> lista = GitHubREST.procurarRepositorios("RepositorioTeste01");
            Assert.IsTrue(lista.Count > 0);
        }

        /// <summary>
        /// Testa o a API que retorna a lista de Contribuidores de um repositório
        /// </summary>
        [TestMethod]
        public void obterContribuidores()
        {
            List<GitHubProprietario> lista = GitHubREST.obterContribuidores("robertosistemas", "RepositorioTeste01");
            Assert.IsTrue(lista.Count > 0);
        }

        /// <summary>
        /// Testa o a API que um repositório
        /// </summary>
        [TestMethod]
        public void obterRepositorio()
        {
            GitHubRepositorio repositorio = GitHubREST.obterRepositorio("robertosistemas", "RepositorioTeste01");
            Assert.IsTrue(repositorio.name.Equals("RepositorioTeste01"));
        }

    }
}
