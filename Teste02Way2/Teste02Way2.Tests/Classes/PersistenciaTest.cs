﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Teste02Way2.Models;

namespace Teste02Way2.Tests.Classes
{
    [TestClass]
    public class PersistenciaTest
    {

        /// <summary>
        /// Inicializa informações necessárias
        /// </summary>
        [TestInitialize]
        public void Inicializa()
        {
            FuncoesTestes.InicializaCaminho();
        }

        /// <summary>
        /// Testa o carregamento de um arquivo texto
        /// </summary>
        [TestMethod]
        public void carregaArquivo()
        {
            string arquivo = Persistencia.CaminhoCompleto("ArquivoTeste.txt");
            string conteudoArquivo = "conteúdo do arquivo";

            Persistencia.salvaArquivo(arquivo, conteudoArquivo);

            string conteudoLido = Persistencia.carregaArquivo(arquivo);

            // Testa se o conteúdo salvo e lido são IGUAIS
            Assert.AreEqual(conteudoArquivo, conteudoLido);

        }

        /// <summary>
        /// Testa o salvamento de um arquivo
        /// </summary>
        [TestMethod]
        public void salvaArquivo()
        {
            string arquivo = Persistencia.CaminhoCompleto("ArquivoTeste.txt");
            Persistencia.salvaArquivo(arquivo, "conteúdo do arquivo");
        }

        /// <summary>
        /// Testa o retorno do caminho completo de um arquivo
        /// </summary>
        [TestMethod]
        public void CaminhoCompleto()
        {
            string caminho1 = System.IO.Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(),"Favoritos.json");
            string caminho2 = Persistencia.CaminhoCompleto("Favoritos.json");

            // Testa se caminhos dos arquivos são IGUAIS
            Assert.AreEqual(caminho1, caminho1);

        }

    }
}
