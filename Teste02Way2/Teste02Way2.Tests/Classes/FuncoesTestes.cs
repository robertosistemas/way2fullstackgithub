﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Mvc;

namespace Teste02Way2.Tests.Classes
{
    public class FuncoesTestes
    {
        public static void InicializaCaminho()
        {
            string caminhoRelativo = AppDomain.CurrentDomain.BaseDirectory;
            int pos = caminhoRelativo.IndexOf(".Tests\\");
            string caminhoCompleto = System.IO.Path.Combine(caminhoRelativo.Substring(0, pos), "App_Data");
            // Inicializa o DataDirectory para ler e salvar arquivos favoritos
            AppDomain.CurrentDomain.SetData("DataDirectory", caminhoCompleto);
        }


        /// <summary>
        /// Verifica se está ocorrendo alguma exceção no controller que está sendo tratada e direcionada para viewer ERROR
        /// </summary>
        /// <param name="resultado"></param>
        public static void verificaExcecoesTratadas(ViewResult resultado)
        {
            if (resultado != null)
            {
                HandleErrorInfo erro = resultado.Model as HandleErrorInfo;
                if (erro != null)
                {
                    Assert.Fail(erro.Exception.StackTrace.ToString());
                }
            }
        }

    }
}
