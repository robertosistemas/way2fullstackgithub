﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Teste02Way2.Models;
using Teste02Way2.Tests.Classes;

namespace Teste02Way2.Tests.Controllers
{
    [TestClass]
    public class GitHubRepositorioDALTest
    {

        /// <summary>
        /// Inicializa informações necessárias
        /// </summary>
        [TestInitialize]
        public void Inicializa()
        {
            // True para fazer cache local e False para sempre buscar no GitHub
            GitHubREST.UtilizarCacheLocalParaTestes = true;

            FuncoesTestes.InicializaCaminho();
        }

        /// <summary>
        /// Testa a obtenção da lista de repositórios do candidado
        /// </summary>
        [TestMethod]
        public void obterRepositoriosCandidato()
        {
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                List<GitHubRepositorio> lista = dal.obterRepositoriosCandidato("robertosistemas");
                Assert.IsTrue(lista.Count > 0);
            }
        }

        /// <summary>
        /// Testa a procura de repositórios por parte do nome
        /// </summary>
        [TestMethod]
        public void procurarRepositorios()
        {
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                List<GitHubRepositorio> lista = dal.procurarRepositorios("RepositorioTeste01");
                Assert.IsTrue(lista.Count > 0);
            }
        }

        /// <summary>
        /// Testa a obtenção de um único repositório
        /// </summary>
        [TestMethod]
        public void obterRepositorio()
        {
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                GitHubRepositorio repositorio = dal.obterRepositorio("robertosistemas", "RepositorioTeste01");
                Assert.IsTrue(repositorio.name.Equals("RepositorioTeste01"));
            }
        }

        /// <summary>
        /// Testa a obtenção da lista de repositórios favoritos
        /// </summary>
        [TestMethod]
        public void obterRepositoriosFavoritos()
        {
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                List<GitHubRepositorio> lista = dal.obterRepositoriosFavoritos();
            }
        }

        /// <summary>
        /// Testa o salvamento de um repositório nos favoritos
        /// </summary>
        [TestMethod]
        public void salvarRepositorioFavorito()
        {
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                dal.salvarRepositorioFavorito("robertosistemas", "RepositorioTeste01");
            }
        }

        /// <summary>
        /// Testa a exclusão de um repositório dos favoritos
        /// </summary>
        [TestMethod]
        public void excluirRepositorioFavorito()
        {
            // Salva para depois excluir
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                dal.salvarRepositorioFavorito("robertosistemas", "RepositorioTeste10");
            }
            // Testa a EXCLUSÃO
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                dal.excluirRepositorioFavorito("robertosistemas", "RepositorioTeste10");
            }
        }

        /// <summary>
        /// Testa a verificação se já está salvo ou não nos favoritos
        /// </summary>
        [TestMethod]
        public void verificarJaSalvo()
        {
            // Salva para depois verificar se está salvo
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                dal.salvarRepositorioFavorito("robertosistemas", "RepositorioTeste10");
            }
            // se retornar True significa que está salvo
            using (GitHubRepositorioDAL dal = new GitHubRepositorioDAL())
            {
                Assert.IsTrue(dal.verificarJaSalvo("robertosistemas", "RepositorioTeste10"));
            }
        }

    }
}