﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Teste02Way2.Models;

namespace Teste02Way2.Tests.Classes
{
    [TestClass]
    public class UtilTest
    {
        /// <summary>
        /// Testa a geração de Md5 Hash
        /// </summary>
        [TestMethod]
        public void GetMd5Hash()
        {
            string texto1 = "way2";
            string texto2 = "technologia";
            string texto3 = "way2";

            string Md5Hash1 = Util.GetMd5Hash(texto1);
            string Md5Hash2 = Util.GetMd5Hash(texto2);
            string Md54Hash3 = Util.GetMd5Hash(texto3);

            // Testa se os Hash Calculados são IGUAIS
            Assert.AreEqual(Md5Hash1, Md54Hash3);
            // Testa se os Hash Calculados são DIFERENTES
            Assert.AreNotEqual(Md5Hash1, Md5Hash2);
            // Testa se os Hash Calculados são DIFERENTES
            Assert.AreNotEqual(Md5Hash2, Md54Hash3);

        }
    }
}
