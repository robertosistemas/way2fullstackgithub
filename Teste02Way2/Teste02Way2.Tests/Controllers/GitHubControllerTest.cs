﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Mvc;
using Teste02Way2.Controllers;
using Teste02Way2.Models;
using Teste02Way2.Tests.Classes;

namespace Teste02Way2.Tests.Controllers
{
    [TestClass]
    public class GitHubControllerTest
    {

        /// <summary>
        /// Inicializa informações necessárias
        /// </summary>
        [TestInitialize]
        public void Inicializa()
        {
            // True para fazer cache local e False para sempre buscar no GitHub
            GitHubREST.UtilizarCacheLocalParaTestes = true;

            FuncoesTestes.InicializaCaminho();
        }

        /// <summary>
        /// Testa a action que retorna os repositórios favoritos
        /// </summary>
        [TestMethod]
        public void MeusRepositorios()
        {
            // Arrange
            GitHubController controller = new GitHubController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);

            // Verifica se ocorreu alguma exceção no controller que está sendo direcionado para viewer ERROR
            FuncoesTestes.verificaExcecoesTratadas(result);

            List<GitHubRepositorio> lista = result.Model as List<GitHubRepositorio>;
            // Vai existir pelo menos um repositório no github de robertosistemas;
            Assert.IsTrue(lista.Count > 0);

        }

        /// <summary>
        /// Testa a action que procura repositórios no github por partes do nome
        /// </summary>
        [TestMethod]
        public void ProcurarRepositorios()
        {
            // Arrange
            GitHubController controller = new GitHubController();

            // Act
            ViewResult result = controller.ProcurarRepositorios("linux") as ViewResult;

            // Assert
            Assert.IsNotNull(result);

            // Verifica se ocorreu alguma exceção no controller que está sendo direcionado para viewer ERROR
            FuncoesTestes.verificaExcecoesTratadas(result);

            List<GitHubRepositorio> lista = result.Model as List<GitHubRepositorio>;

            // Vai existir alguns repositórios com o nome de linux e pelo menos um com o nome completo de torvalds/linux
            Assert.IsTrue(lista.Exists((item) => item.full_name.Equals("torvalds/linux")));

            // Não existirá algum repositórios com o nome completo de robertosistemas/linux
            Assert.IsFalse(lista.Exists((item) => item.full_name.Equals("robertosistemas/linux")));

        }

        /// <summary>
        /// Testa a action que procura repositórios no github por partes do nome
        /// </summary>
        [TestMethod]
        public void RepositoriosFavoritos()
        {
            // Arrange
            GitHubController controller = new GitHubController();

            // Salva alguns repositórios como favoritos
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste01");
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste02");
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste03");

            // Consulta todos os repositórios salvos como favoritos
            ViewResult resultFavoritos01 = controller.RepositoriosFavoritos() as ViewResult;
            Assert.IsNotNull(resultFavoritos01);

            // Obtêm a lista de repositórios a partir do modelo
            List<GitHubRepositorio> lista01 = resultFavoritos01.Model as List<GitHubRepositorio>;

            // Verifica se os repositórios estão salvos nos favoritos
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste01")));
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste02")));
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste03")));

        }

        /// <summary>
        /// Testa a action que exibe detalhes de um repositório
        /// </summary>
        [TestMethod]
        public void Detalhes()
        {
            // Arrange
            GitHubController controller = new GitHubController();

            // Consulta informações detalhadas de algum repositório no github
            ViewResult result01 = controller.Detalhes("jquery", "jquery") as ViewResult;

            Assert.IsNotNull(result01);

            // Verifica se ocorreu alguma exceção no controller que está sendo direcionado para viewer ERROR
            FuncoesTestes.verificaExcecoesTratadas(result01);

            // Obtêm a entidade repositórios a partir do modelo
            GitHubRepositorio repositorio01 = result01.Model as GitHubRepositorio;

            // Verifica se encontrou
            Assert.IsTrue(repositorio01.name.Equals("jquery"));

        }

        /// <summary>
        /// Testa a action que salva um repositório nos favoritos
        /// </summary>
        [TestMethod]
        public void SalvarRepositorio()
        {
            // Arrange
            GitHubController controller = new GitHubController();

            // Salva alguns repositórios como favoritos
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste01");
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste02");
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste03");

            // Consulta todos os repositórios salvos como favoritos
            ViewResult resultFavoritos01 = controller.RepositoriosFavoritos() as ViewResult;

            Assert.IsNotNull(resultFavoritos01);

            // Verifica se ocorreu alguma exceção no controller que está sendo direcionado para viewer ERROR
            FuncoesTestes.verificaExcecoesTratadas(resultFavoritos01);


            // Obtêm a lista de repositórios a partir do modelo
            List<GitHubRepositorio> lista01 = resultFavoritos01.Model as List<GitHubRepositorio>;

            // Verifica se os repositórios foram SALVOS
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste01")));
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste02")));
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste03")));

        }

        /// <summary>
        /// Testa a action que Exclui um repositório dos favoritos
        /// </summary>
        [TestMethod]
        public void ExcluirRepositorio()
        {
            // Arrange
            GitHubController controller = new GitHubController();

            // Salva alguns repositórios como favoritos
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste01");
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste02");
            controller.SalvarRepositorio("robertosistemas", "RepositorioTeste03");

            // Consulta todos os repositórios salvos como favoritos
            ViewResult resultFavoritos01 = controller.RepositoriosFavoritos() as ViewResult;
            Assert.IsNotNull(resultFavoritos01);

            // Obtêm a lista de repositórios a partir do modelo
            List<GitHubRepositorio> lista01 = resultFavoritos01.Model as List<GitHubRepositorio>;

            // Verifica se os repositórios foram salvos
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste01")));
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste02")));
            Assert.IsTrue(lista01.Exists((item) => item.name.Equals("RepositorioTeste03")));

            // Exclui alguns repositórios
            controller.ExcluirRepositorio("robertosistemas", "RepositorioTeste01");
            controller.ExcluirRepositorio("robertosistemas", "RepositorioTeste02");
            controller.ExcluirRepositorio("robertosistemas", "RepositorioTeste03");

            // Consulta todos os repositórios novamente após as exclusões
            ViewResult resultFavoritos02 = controller.RepositoriosFavoritos() as ViewResult;
            Assert.IsNotNull(resultFavoritos02);

            // Obtêm a lista de repositórios a partir do modelo
            List<GitHubRepositorio> lista02 = resultFavoritos01.Model as List<GitHubRepositorio>;

            // Verifica se os repositórios foram EXCLUÍDOS corretamente
            Assert.IsFalse(lista02.Exists((item) => item.name.Equals("RepositorioTeste01")));
            Assert.IsFalse(lista02.Exists((item) => item.name.Equals("RepositorioTeste02")));
            Assert.IsFalse(lista02.Exists((item) => item.name.Equals("RepositorioTeste03")));
        }

    }
}
